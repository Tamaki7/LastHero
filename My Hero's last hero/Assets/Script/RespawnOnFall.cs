using UnityEngine;

public class RespawnOnFall : MonoBehaviour
{
    // El punto de respawn
    public Vector3 respawnPoint = new Vector3(0, 0, 0);

    // La altura m�nima permitida antes de ser teleportado
    public float minHeight = -10.0f;

    // El collider del personaje
    private Collider m_collider;

    void Start()
    {
        // Obtiene el collider del personaje
        m_collider = GetComponent<Collider>();

        // Establece el punto de respawn en el inicio
        respawnPoint = transform.position;
    }

    void Update()
    {
        // Verifica si el personaje ha ca�do por debajo de la altura m�nima permitida
        if (transform.position.y < minHeight)
        {
            // Mueve al personaje al punto de respawn
            transform.position = respawnPoint;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        // Verifica si el personaje ha colisionado con la superficie
        if (collision.collider == m_collider)
        {
            // Actualiza el punto de respawn al punto actual del personaje
            respawnPoint = transform.position;
        }
    }
}
